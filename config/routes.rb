Rails.application.routes.draw do

  resources :subjects  do
    get :subject_teacher_list,on: :collection
    get :assign_teachers,on: :collection
    post :create_teachers_subject,on: :collection
  end	
  resources :students do 
    post :student_mark_create,on: :collection
    get :mark_form, on: :collection
  end  
  root :to => "sessions#new"
  resources :users
  resources :sessions, only: [:new, :create, :destroy] 
  get "login", to: 'sessions#new', as: 'login'
  get "logout", to: 'sessions#destroy', as: 'logout'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
