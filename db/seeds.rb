Role.create!([
  {name: "Admin", status: true},
  {name: "Teacher", status: true}
])
Student.create!([
  {name: "student_one", student_id: "ST001"},
  {name: "student_two", student_id: "ST002"},
  {name: "student_three", student_id: "ST003"},
  {name: "student four", student_id: "ST04"}
])
Subject.create!([
  {name: "Tamil", code: "TA001"},
  {name: "English", code: "EN001"},
  {name: "Maths", code: "MT001"},
  {name: "Science", code: "SC001",},
  {name: "SocialScience", code: "SS001"}
])
