class AddColumnToStudents < ActiveRecord::Migration[5.1]
  def change
    add_column :students, :rank, :integer
    add_column :students, :total, :integer
  end
end
