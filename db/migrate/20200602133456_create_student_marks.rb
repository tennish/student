class CreateStudentMarks < ActiveRecord::Migration[5.1]
  def change
    create_table :student_marks do |t|
      t.belongs_to :student, foreign_key: true
      t.belongs_to :subject, foreign_key: true
      t.integer :mark

      t.timestamps
    end
  end
end
