class Student < ApplicationRecord
	has_many :student_marks
	validates :name, presence: true
	validates :student_id, presence: true,uniqueness: true

	def self.calculate_rank
    total = ""
		rank = ""
		Student.all.order(total: :desc).each_with_index do |student,index|
		  if student.total == total
		    rank = rank
		  else
		    rank = index+1
		  end  
		  student.update(rank: rank)
		  total = student.total  
		end
	end	
	
end
