class Role < ApplicationRecord
	has_many :users
	scope :teacher, -> { joins(:users).where(name: "Teacher") }
end
