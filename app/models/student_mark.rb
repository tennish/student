class StudentMark < ApplicationRecord
  belongs_to :student
  belongs_to :subject
  after_save :update_total

  def update_total
    student = Student.find(self.student_id) 
    student.update(total: student.student_marks.pluck(:mark).compact.sum)
  end 
end
