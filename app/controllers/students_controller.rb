class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  # GET /students
  # GET /students.json
  def index
    @students = Student.all.order(rank: :asc)
    @subjects = Subject.all
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def mark_form
    @students = Student.all
    @teacher = current_user
    @student_mark = StudentMark.new
    @subject = current_user.user_subject.subject
  end

  def student_mark_create
    student_id = student_mark_params[:student_id]
    mark = student_mark_params[:mark]
    student_mark_ids = student_mark_params[:id]
    student_id.each_with_index do |stud_id,index|
      if student_mark_ids[index] == ""
        StudentMark.create(student_id: stud_id,mark: mark[index],subject_id: current_user.user_subject.subject.id)
      else
        StudentMark.find(student_mark_ids[index]).update(mark: mark[index])
      end  
    end
    Student.calculate_rank
    redirect_to students_url  
  end  
  
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def student_params
      params.require(:student).permit(:name, :student_id, :status)
    end

    def student_mark_params
      params.require(:student_mark).permit(id: [], student_id: [], mark: [])
    end
end
