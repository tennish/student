class SubjectsController < ApplicationController
  before_action :set_subject, only: [:show, :edit, :update, :destroy]

  # GET /subjects
  # GET /subjects.json
  def index
    @subjects = Subject.all
  end

  # GET /subjects/1
  # GET /subjects/1.json
  def show
  end

  # GET /subjects/new
  def new
    @subject = Subject.new
  end

  # GET /subjects/1/edit
  def edit
  end

  # POST /subjects
  # POST /subjects.json
  def create
    @subject = Subject.new(subject_params)

    respond_to do |format|
      if @subject.save
        format.html { redirect_to @subject, notice: 'Subject was successfully created.' }
        format.json { render :show, status: :created, location: @subject }
      else
        format.html { render :new }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subjects/1
  # PATCH/PUT /subjects/1.json
  def update
    respond_to do |format|
      if @subject.update(subject_params)
        format.html { redirect_to @subject, notice: 'Subject was successfully updated.' }
        format.json { render :show, status: :ok, location: @subject }
      else
        format.html { render :edit }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subjects/1
  # DELETE /subjects/1.json
  def destroy
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to subjects_url, notice: 'Subject was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def subject_teacher_list
   @user_subjects = UserSubject.all
  end  

  def assign_teachers
    @user_subject = UserSubject.new
    @teachers = User.where(role_id: Role.find_by_name("Teacher"))
    @subjects = Subject.all
  end

  def create_teachers_subject
    @user_subject = UserSubject.new(user_subject_params)

    respond_to do |format|
      if @user_subject.save
        format.html { redirect_to subject_teacher_list_subjects_url , notice: 'Successfully Assigned subject to Teacher.' }
      else
        format.html { redirect_to subject_teacher_list_subjects_url , notice: 'Already Assigned this Subject' }
      end
    end
  end  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      @subject = Subject.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def subject_params
      params.require(:subject).permit(:name, :code, :status)
    end

    def user_subject_params
      params.require(:user_subject).permit(:user_id, :subject_id)
    end

end
