json.extract! user, :id, :name, :email, :password, :role_id, :status, :created_at, :updated_at
json.url user_url(user, format: :json)
