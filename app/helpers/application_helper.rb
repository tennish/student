module ApplicationHelper
	
	def role_teacher_id
    Role.find_by_name("Teacher").id
	end

	def check_admin(role_id)
    Role.find(role_id).name == "Admin"
	end	
	
end
