module StudentsHelper
  
  def student_mark(student_id,subject_id)
    StudentMark.where(student_id: student_id,subject_id: subject_id).present? ? StudentMark.find_by(student_id: student_id,subject_id: subject_id).mark : "" 
  end	

  def student_mark_id(student_id,subject_id)
    StudentMark.where(student_id: student_id,subject_id: subject_id).present? ? StudentMark.find_by(student_id: student_id,subject_id: subject_id).id : "" 
  end	

end
